/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import com.gitlab.util.ExternalizeUtil;
import com.gitlab.util.Slugifier;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class Category implements Externalizable {
    
    public static final String MAIN_CATEGORY_SLUG = "";
    public static final String MAIN_CATEGORY_TITLE = "";
    public static final String MAIN_CATEGORY_ALIAS = "/";

    private String title;
    private String description;

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        LinkedHashMap m = new LinkedHashMap();
        
        m.put("title", title);
        m.put("description", description);
        
        ExternalizeUtil.writeAsJSON(out, m);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Map<String, String> m = ExternalizeUtil.readInJSON(in);
        
        title = m.get("title");
        description = m.get("description");
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.title);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        return true;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (MAIN_CATEGORY_ALIAS.equals(title.trim().toLowerCase())) {
            this.title = MAIN_CATEGORY_TITLE;
        }
        else {
            this.title = title;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
 
    public static String getSlug(String title) {
        if (MAIN_CATEGORY_TITLE.equals(title)) {
            return MAIN_CATEGORY_SLUG;
        }
        
        if (title.matches("^\\d+$")) {
            return "_" + Slugifier.slugify(title);
        }

        return Slugifier.slugify(title);
    }

    public String getSlug() {
        return getSlug(title);
    }
    
    public boolean isMain() {
        return MAIN_CATEGORY_TITLE.equals(title);
    }
    
}
