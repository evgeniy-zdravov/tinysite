/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite.app.controllers;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import org.json.simple.JSONValue;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import com.gitlab.tinysite.app.WebApp;
import com.gitlab.tinysite.app.WebContext;
import com.gitlab.tinysite.storage.CollectionsStorageEngine;
import com.gitlab.tinysite.Article;
import com.gitlab.tinysite.User;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class ApiTest {
    
    public ApiTest() {
    }
    
    static WebApp webApp;
    
    @BeforeClass
    public static void startWebApp() {
        webApp = new WebApp();
        webApp.properties = new Properties();
        webApp.context = new WebContext(new Properties(), new CollectionsStorageEngine());
        
        webApp.context.users.createUser(User.fromIdPassword("test", "testtest1234"));
        
        webApp.init();
        
        spark.Spark.awaitInitialization();
    }
    
    @AfterClass
    public static void stopWebApp() {
        webApp.stop();
    }

    public static void main(String[] args) {
        startWebApp();
    }

    private static final Logger LOG = Logger.getLogger(ApiTest.class.getName());
    
    private static Map<String, ?> queryMap(String method, String path, Map<String, Object> params) {
        return (Map)query(method, path, params);
    }

    private static <T> T query(String method, String path, Map<String, Object> params) {
        try {
            String queryString = "";
            
            if (!params.isEmpty()) {
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    if (!queryString.isEmpty())
                        queryString += '&';
                    
                    queryString += URLEncoder.encode(param.getKey(), StandardCharsets.UTF_8) + '=' + URLEncoder.encode(String.valueOf(param.getValue()), StandardCharsets.UTF_8);
                }
            }

            String url;
            
            url = "http://localhost:" + spark.Spark.port() + path;
            
            if ("GET".equals(method) && !queryString.isEmpty()) {
                url += '?' + queryString;
            }
            
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            
            if (method != null && !"GET".equals(method)) {
                conn.setRequestMethod(method);
                conn.setDoOutput(true);
                try (OutputStream out = conn.getOutputStream()) {
                    out.write(queryString.getBytes(StandardCharsets.UTF_8));
                }
            }

            try (InputStreamReader r = new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8)) {
                return (T) JSONValue.parse(r);
            }
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Test
    public void testAuth() {
        assertTrue(queryMap("POST", "/api/auth", Map.of("username", "test", "password", "testtest1234")).containsKey("token"));
        assertFalse(queryMap("POST", "/api/auth", Map.of("username", "test", "password", "badpassword")).containsKey("token"));
        assertFalse(queryMap("POST", "/api/auth", Map.of("username", "baduser", "password", "testtest1234")).containsKey("token"));
        assertFalse(queryMap("POST", "/api/auth", Map.of("username", "baduser", "password", "badpassword")).containsKey("token"));
    }

    @Test
    public void testLogout() {
        String token = (String) queryMap("POST", "/api/auth", Map.of("username", "test", "password", "testtest1234")).get("token");
        
        assertNotNull(token);
        assertFalse(token.isEmpty());
        
        assertTrue((Boolean) queryMap("POST", "/api/logout", Map.of("token", token)).get("success"));
    }
    
    private void addArticle(String title, String body, String[] categories, long timestamp) {
        User user = webApp.context.users.getUserById("test");
        Article article = new Article();
        
        article.setAuthorId(user.getId());
        article.setTitle(title);
        article.setBody(body);
        article.setType("text/plain");
        article.setCategories(categories);
        article.setTimestampMillis(timestamp);
        
        webApp.context.articles.post(article);
    }
    
    @Test
    public void testFeed() {
        addArticle("test1", "test text", ",1,2".split(","), 100);
        addArticle("test2", "test text", ",1,2".split(","), 200);
        
        List<Map> feed;
        Map m;
        
        feed = (List<Map>) query("GET", "/api/feed", Collections.EMPTY_MAP);
        assertEquals(feed.size(), 2);
        m = feed.get(0);
        assertEquals(m.get("title"), "test1");
        assertEquals(m.get("body"), "test text");
        
        feed = (List<Map>) query("GET", "/api/feed?limit=1", Collections.EMPTY_MAP);
        assertEquals(feed.size(), 1);
        m = feed.get(0);
        assertEquals(m.get("title"), "test1");
        assertEquals(m.get("body"), "test text");
        
        feed = (List<Map>) query("GET", "/api/feed?after=100", Collections.EMPTY_MAP);
        assertEquals(feed.size(), 1);
        m = feed.get(0);
        assertEquals(m.get("title"), "test2");
        assertEquals(m.get("body"), "test text");
        
        feed = (List<Map>) query("GET", "/api/feed?before=200", Collections.EMPTY_MAP);
        assertEquals(feed.size(), 1);
        m = feed.get(0);
        assertEquals(m.get("title"), "test1");
        assertEquals(m.get("body"), "test text");
    }
    
    @Test
    public void testPost() {
        String token = (String) queryMap("POST", "/api/auth", Map.of("username", "test", "password", "testtest1234")).get("token");

        Map postQuery = new LinkedHashMap();
        
        postQuery.put("token", token);
        postQuery.put("title", "test1");
        postQuery.put("type", "text/plain");
        postQuery.put("body", "test1 text");
        postQuery.put("categories", ".\nposttest");
        
        Map result;
        
        assertEquals(webApp.context.articles.categoryArticleIds("posttest").size(), 0);
        
        result = queryMap("POST", "/api/post", postQuery);
        assertTrue((Boolean)result.get("success"));
        assertEquals(webApp.context.articles.categoryArticleIds("posttest").size(), 1);
        
        postQuery.put("title", "test2");
        
        postQuery.put("token", "123");
        result = queryMap("POST", "/api/post", postQuery);
        assertFalse((Boolean)result.get("success"));
        assertEquals(webApp.context.articles.categoryArticleIds("posttest").size(), 1);
        
        postQuery.remove("token", "123");
        result = queryMap("POST", "/api/post", postQuery);
        assertFalse((Boolean)result.get("success"));
        assertEquals(webApp.context.articles.categoryArticleIds("posttest").size(), 1);
        
        postQuery.remove("token", null);
        result = queryMap("POST", "/api/post", postQuery);
        assertFalse((Boolean)result.get("success"));
        assertEquals(webApp.context.articles.categoryArticleIds("posttest").size(), 1);
    }
    
}
