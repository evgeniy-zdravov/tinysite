/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.util;

import java.security.SecureRandom;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class RandomUtil {

    private static SecureRandom secureRandom = new SecureRandom();
    
    private static char[] ALLOWED_RANDOM_STING_CHARS = "0123456789QWERTYUIOP{}ASDFGHJKL:ZXCVBNM<>?qwertyuiop[]asdfghjkl;zxcvbnm,./".toCharArray();

    public static String string(int length) {
        char[] chars = new char[length];

        for(int i = 0; i < chars.length; i++) {
            chars[i] = ALLOWED_RANDOM_STING_CHARS[secureRandom.nextInt(ALLOWED_RANDOM_STING_CHARS.length)];
        }

        return new String(chars);
    }
    
}
