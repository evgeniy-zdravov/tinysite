/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.gitlab.util.Slugifier;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class DbArticles {
    
    private StorageEngine storageEngine;
    
    private DbUsers users;
    public Map<String, Category> slugCategories;
    public Map<String, Article> articles;
    public Map<String, List<String>> categoryArticles = new ConcurrentHashMap<>();

    public DbArticles(DbUsers users, StorageEngine storageEngine) {
        this.storageEngine = storageEngine;
        this.users = users;
        slugCategories = storageEngine.map("categories");
        articles = storageEngine.map("articles");
    }
    
    public Category createCategoryFromTitle(String title) {
        return slugCategories.computeIfAbsent(Slugifier.slugify(title), (title_) -> {
            Category category = new Category();

            category.setTitle(title);

            return category;
        });
    }
    
    public List<String> categoryArticleIds(String categorySlug) {
        if (categorySlug == null) {
            categorySlug = "";
        }
        return categoryArticles.computeIfAbsent(categorySlug, (slug) -> storageEngine.list("articleIds:" + slug));
    }
    
    public List<Article> categoryArticles(String categorySlug) {
        List<String> ids = categoryArticleIds(categorySlug);
        
        return new AbstractList<Article>() {

            @Override
            public Article get(int index) {
                return articles.get(ids.get(index));
            }

            @Override
            public int size() {
                return ids.size();
            }

        };
    }

    public void post(Article article) {
        if (article.getType().isEmpty()) {
            throw new IllegalArgumentException("content type not specified");
        }
        if (article.getBody().isEmpty()) {
            throw new IllegalArgumentException("no body");
        }
        if (article.getCategories().length == 0) {
            throw new IllegalArgumentException("no categories");
        }
        
        if (article.getTimestampMillis() == 0) {
            article.setTimestampMillis(System.currentTimeMillis());
        }
        
        String id = article.calculateId();
        
        if (articles.putIfAbsent(id, article) != null) {
            throw new IllegalStateException("Duplicate article");
        }
        
        for (String categoryTitle : article.getCategories()) {
            categoryArticleIds(createCategoryFromTitle(categoryTitle).getTitle()).add(id);
    
            clearCategoryListCache();
        }
    }
    
    public void delete(String articleId) {
        Article article = articles.get(articleId);
        
        for (String categoryTitle : article.getCategories()) {
            List<String> categoryIds = categoryArticleIds(createCategoryFromTitle(categoryTitle).getTitle());
            categoryIds.remove(articleId);
            if (categoryIds.isEmpty()) {
                slugCategories.remove(Category.getSlug(categoryTitle));
            }
        }

        clearCategoryListCache();
    }
    
    private void clearCategoryListCache() {
        categoryListCached = null;
        categoryListExceptMainCached = null;
    }
    
    List<Category> categoryListCached = null;

    public List<Category> listCategories() {
        if (categoryListCached == null) {
            Category[] categoryList = slugCategories.values().toArray(new Category[slugCategories.size()]);

            Arrays.sort(categoryList, (a, b) -> Integer.compare(categoryArticleIds(b.getTitle()).size(), categoryArticleIds(a.getTitle()).size()));

            categoryListCached = Arrays.asList(categoryList);
        }
        
        return categoryListCached;
    }

    List<Category> categoryListExceptMainCached = null;

    public List<Category> listCategoriesExceptMain() {
        if (categoryListExceptMainCached == null) {
            List<Category> full = listCategories();

            int mainIndex = -1;
            for(int i = 0; i < full.size(); i++) {
                if (full.get(i).isMain()) {
                    mainIndex = i;
                    break;
                }
            }
            
            List<Category> list;
            
            if (mainIndex == -1) {
                list = full;
            }
            else {
                list = new ArrayList<>(full.size() - 1);
                list.addAll(full.subList(0, mainIndex));
                list.addAll(full.subList(mainIndex + 1, full.size()));
            }
            
            categoryListExceptMainCached = list;
        }
        
        return categoryListExceptMainCached;
    }

    /**
     * 
     * @param categorySlug (null for main category)
     * @return 
     */
    public Category category(String categorySlug) {
        if (categorySlug == null) {
            categorySlug = "";
        }
        
        return slugCategories.get(categorySlug);
    }

}
