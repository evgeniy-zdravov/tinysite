/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class User implements Externalizable {

    public static User fromIdPassword(String id, String password) {
        User u = new User();
        
        u.setId(id);
        u.setPassword(password);
        
        return u;
    }

    protected String id;
    protected byte[] salt;
    protected byte[] hash;
    protected Set<String> groups = new LinkedHashSet<>();

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeByte(1);
        out.writeUTF(id);
        out.writeInt(salt.length);
        out.write(salt);
        out.writeInt(hash.length);
        out.write(hash);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        if (in.readByte() != 1) {
            throw new IOException("Invalid version");
        }
        
        id = in.readUTF();
        
        int l;
        
        l = in.readInt();
        salt = new byte[l];
        in.readFully(salt);

        l = in.readInt();
        hash = new byte[l];
        in.readFully(hash);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if (id.isEmpty()) {
            throw new IllegalArgumentException("Empty usernames not allowed");
        }
        
        int len = id.length();
        for(int i = 0; i < len; i++) {
            char c = id.charAt(i);
            
            if ((c < 'a' || c > 'z') && c != '_' && c != '.') {
                throw new IllegalArgumentException("Only lowercase latin letters, underscore and dot are allowed in username");
            }
        }
        
        this.id = id;
    }
    
    private static byte[] getHash(String password, byte[] salt) {
        MessageDigest md;

        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            throw new Error(ex);
        }
        
        md.update(salt);
        
        return md.digest(password.getBytes(StandardCharsets.UTF_8));
    }
    
    public boolean isPasswordCorrect(String password) {
        return Arrays.equals(getHash(password, salt), hash);
    }
    
    private static SecureRandom secureRandom = new SecureRandom();

    public void setPassword(String password) {
        if (password.length() < 10) {
            throw new IllegalArgumentException("Password too short");
        }
        
        salt = new byte[12];
        secureRandom.nextBytes(salt);
        hash = getHash(password, salt);
    }
    
}
