/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite.app;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.thread.ThreadPool;
import spark.servlet.SparkApplication;
import static spark.Spark.*;
import spark.embeddedserver.EmbeddedServers;
import spark.embeddedserver.jetty.EmbeddedJettyServer;
import spark.embeddedserver.jetty.JettyHandler;
import spark.embeddedserver.jetty.JettyServerFactory;
import spark.http.matching.MatcherFilter;
import spark.route.Routes;
import spark.staticfiles.StaticFilesConfiguration;
import com.gitlab.tinysite.app.controllers.Api;
import com.gitlab.tinysite.app.controllers.Html;
import com.gitlab.tinysite.storage.MapDBStorageEngine;


/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class WebApp implements SparkApplication {

    public Properties properties = null;
    public WebContext context = null;
    
    private void initAllowCors() {
        options("/api/*",
        (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before("/api/*", (request, response) -> response.header("Access-Control-Allow-Origin", "*"));
    }
    
    private boolean staticServing = false;
    
    private void embeddedWebServerInit() {
        if (new File("htdocs").isDirectory()) {
            staticFiles.externalLocation("htdocs");
            
            staticServing = true;
        }
        
        ipAddress(properties.getProperty("address", "127.0.0.1"));
        if (properties.containsKey("port")) {
            port(Integer.parseInt(properties.getProperty("port", "4567")));
        }

        EmbeddedServers.add(EmbeddedServers.Identifiers.JETTY, (Routes routeMatcher, StaticFilesConfiguration staticFilesConfiguration, boolean hasMultipleHandler) -> {
            /**
              * setup handler in the same manner spark does in {@code EmbeddedJettyFactory.create()}.
              *
              * @see <a href="https://github.com/perwendel/spark/blob/master/src/main/java/spark/embeddedserver/jetty/EmbeddedJettyFactory.java#L39">EmbeddedJettyFactory.java</a>
              */
            MatcherFilter matcherFilter = new MatcherFilter(routeMatcher, staticFilesConfiguration, false, hasMultipleHandler);
            matcherFilter.init(null);
            JettyHandler handler = new JettyHandler(matcherFilter);
            
            //handler.getSessionCookieConfig().setName("XSESSION");
            
            return new EmbeddedJettyServer(new JettyServerFactory() {
                @Override
                public Server create(int maxThreads, int minThreads, int threadTimeoutMillis) {
                    return new Server();
                }

                @Override
                public Server create(ThreadPool threadPool) {
                    return new Server();
                }
            }, handler);
        });
    }
    
    private Html htmlApp = null;
    
    public void initHTMLApp() {
        new Html(context).init();
    }
    
    @Override
    public void init() {
        if (context == null) {
            context = new WebContext(properties, MapDBStorageEngine.file(new File("tinysite.mapdb")));
        }
        
        htmlApp = new Html(context);
        
        if (!staticServing) {
            htmlApp.initStatic();
        }

        if ("true".equals(properties.getOrDefault("allowCors", "false"))) {
            initAllowCors();
        }

        Api api = new Api(context);
        api.init();

        get("/status", (req, res) -> "OK");

        if (!staticServing) {
            htmlApp.init();
        }
    }

    @Override
    public void destroy() {
    }
    
    public void stop() {
        spark.Spark.stop();
        destroy();
    }

    public static void main(String[] args) throws URISyntaxException, IOException {
        WebApp webApp = new WebApp();
        
        if (webApp.properties == null) {
            webApp.properties = new Properties();
            File propertiesFile = new File("tinysite.properties");
            if (propertiesFile.canRead()) {
                try (Reader in = new InputStreamReader(new FileInputStream(propertiesFile), StandardCharsets.UTF_8)) {
                    webApp.properties.load(in);
                }
            }
        }
        
        webApp.embeddedWebServerInit();

        webApp.init();
        webApp.registerShutdownHook();
        
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().browse(new URI("http://localhost:" + port()));
            }
        }
        catch (Error ex) {
        }
    }

    public void registerShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> destroy()));
    }
}
