/*
 * The MIT License
 *
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.gitlab.tinysite.app.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import spark.ModelAndView;
import spark.Request;
import spark.Session;
import spark.TemplateEngine;
import spark.template.velocity.VelocityTemplateEngine;
import com.gitlab.tinysite.app.WebContext;
import com.gitlab.tinysite.app.controllers.velocityutils.DateTool;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class LayoutRenderer {
    
    public static final String LAYOUT_TEMPLATE_RESOURCE_PREFIX = "/com/gitlab/tinysite/template";
    
    final WebContext context;
    final String root;
    TemplateEngine templateEngine;
    String viewNamePrefix;
    final boolean recreateTemplateEngine;

    public LayoutRenderer(WebContext context, String root) {
        this.context = context;
        this.root = root;

        recreateTemplateEngine = true; // TODO : set to false, when done with templates
        createTemplateEngine();
    }
    
    private void createTemplateEngine() {
        Properties velocityProperties = new Properties();
        
        velocityProperties.setProperty(RuntimeConstants.EVENTHANDLER_INCLUDE, "org.apache.velocity.app.event.implement.IncludeRelativePath");
        
        if (root == null) {
            velocityProperties.setProperty(RuntimeConstants.RESOURCE_LOADER, "class");
            velocityProperties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader"); // just copypasted that from spark.template.velocity.VelocityTemplateEngine
            viewNamePrefix = LAYOUT_TEMPLATE_RESOURCE_PREFIX;
        }
        else {
            velocityProperties.setProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            velocityProperties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader"); // just copypasted that from spark.template.velocity.VelocityTemplateEngine
            velocityProperties.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, root);
            viewNamePrefix = "";
        }

        templateEngine = new VelocityTemplateEngine(new VelocityEngine(velocityProperties));
    }
    
    public String renderError(String message, Request request) {
        return render("/error.vsl", request, Map.of("errorMessage", message));
    }

    public String render(String viewName, Request request) {
        return render(viewName, request, null);
    }
    
    public String render(String viewName, Request request, Map model) {
        if (!(model instanceof HashMap)) {
            model = model == null ? new HashMap<>() : new HashMap<>(model);
        }
        
        model.put("_DateTool", DateTool.INSTANCE);

        model.put("_users", context.users);
        model.put("_articles", context.articles);
        model.put("_viewName", viewName);
        model.put("_request", request);
        
        model.put("props", context.properties);

        Session session = request.session(false);
        if(session == null || session.attribute("_user") == null) {
            model.put("_isLoggedIn", false);
        }
        else {
            model.put("_isLoggedIn", true);
            model.put("_user", session.attribute("_user"));
        }
        
        model.put("categories", context.articles.listCategories());
        model.put("categoriesExceptMain", context.articles.listCategoriesExceptMain());
        
        if (recreateTemplateEngine) {
            createTemplateEngine();
        }
        
        return templateEngine.render(new ModelAndView(model, viewNamePrefix + viewName));
    }
    
}
