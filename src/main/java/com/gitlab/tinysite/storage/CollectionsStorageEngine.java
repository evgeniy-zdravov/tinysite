/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite.storage;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import com.gitlab.tinysite.StorageEngine;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class CollectionsStorageEngine implements StorageEngine {

    private Map<String, Map> maps = new ConcurrentHashMap<>();
    private Map<String, Set> sets = new ConcurrentHashMap<>();
    private Map<String, List> lists = new ConcurrentHashMap<>();

    @Override
    public <K, V> Map<K, V> map(String id) {
        return maps.computeIfAbsent(id, (i) -> new ConcurrentHashMap());
    }

    @Override
    public <V> Set<V> set(String id) {
        return sets.computeIfAbsent(id, (i) -> new ConcurrentHashMap().keySet(Boolean.TRUE));
    }

    @Override
    public <V> List<V> list(String id) {
        return lists.computeIfAbsent(id, (i) -> new Vector());
    }

    @Override
    public void close() {
    }
    
}
