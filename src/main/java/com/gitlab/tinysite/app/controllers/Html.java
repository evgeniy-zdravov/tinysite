/*
 * The MIT License
 *
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.gitlab.tinysite.app.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.Bindings;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import org.json.simple.JSONValue;
import spark.Request;
import spark.Response;
import spark.Session;
import spark.Spark;
import static spark.Spark.staticFiles;
import com.gitlab.tinysite.Article;
import com.gitlab.tinysite.Category;
import com.gitlab.tinysite.User;
import com.gitlab.tinysite.app.WebContext;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class Html {

    private static final Logger LOG = Logger.getLogger(Html.class.getName());
    
    final WebContext context;
    LayoutRenderer layoutRenderer;

    public Html(WebContext context) {
        this.context = context;
    }
    
    private boolean staticInitialized = false;
    
    public void initStatic() {
        staticInitialized = true;
        staticFiles.location(LayoutRenderer.LAYOUT_TEMPLATE_RESOURCE_PREFIX);
    }

    public void init() {
        if (!staticInitialized)
            initStatic();

        layoutRenderer = new LayoutRenderer(context, null);

        Spark.get("/", (req, resp) -> category(req, resp));
        Spark.get("/magic", (req, resp) -> magic(req, resp));
        Spark.post("/magic", (req, resp) -> magicCast(req, resp));
        Spark.get("/login", (req, resp) -> loginForm(req, resp));
        Spark.post("/login", (req, resp) -> login(req, resp));
        Spark.post("/logout", (req, resp) -> logout(req, resp));
        Spark.post("/post", (req, resp) -> post(req, resp));
        Spark.post("/delete/:postId", (req, resp) -> delete(req, resp));
        Spark.get("/:categoryOrPageNum", (req, resp) -> category(req, resp));
        Spark.get("/:category/:pageNum", (req, resp) -> category(req, resp));
    }
    
    public static int articlesPerPage = 10;

    public String category(Request req, Response resp) {
        String categorySlugOrPageNum = req.params("categoryOrPageNum");
        
        String categorySlug = null;
        int skipPages = 0;
        
        if (categorySlugOrPageNum != null)
            try {
                skipPages = Integer.parseInt(categorySlugOrPageNum) - 1;
            }
            catch (NumberFormatException ex) {
                categorySlug = categorySlugOrPageNum;
            }
        else {
            categorySlug = req.params("category");
            
            String pageNumStr = req.params("pageNum");
            
            if (pageNumStr != null) {
                skipPages = Integer.parseInt(pageNumStr) - 1;
            }
        }
        
        Category category = context.articles.category(categorySlug);
        
        if (category == null && categorySlug == null) {
            category = context.articles.createCategoryFromTitle(Category.MAIN_CATEGORY_TITLE);
        }
        
        if (category == null) {
            Spark.halt(404, layoutRenderer.render(
                "/error.vsl",
                req,
                Map.of(
                    "errorMessage", "Category missing"
                )
            ));
        }
        
        int start = skipPages * articlesPerPage;
        int end = start + articlesPerPage;
        
        List<Article> categoryArticles = layoutRenderer.context.articles.categoryArticles(category.getTitle());
        
        int pageCount = (int) Math.ceil((double)categoryArticles.size() / (double)articlesPerPage);
        
        ArrayList<Article> pageArticles = new ArrayList<>(articlesPerPage);
        
        start = categoryArticles.size() - start;
        end = categoryArticles.size() - end;
        
        if (start < 0)
            start = 0;
        
        if (end < 0)
            end = 0;
        
        pageArticles.addAll(categoryArticles.subList(end, start));
        Collections.reverse(pageArticles);
        
        return layoutRenderer.render(
            "/feed.vsl",
            req,
            Map.of(
                "feed", pageArticles,
                "category", category,
                "pageCount", pageCount,
                "skipPages", skipPages
            )
        );
    }
    
    public String loginForm(Request req, Response resp) {
        return layoutRenderer.render(
            "/login.vsl",
            req
        );
    }

    public String login(Request req, Response resp) {
        String username = req.queryParams("username");
        String password = req.queryParams("password");
        
        User user = context.users.getUserById(username);
        
        if (user == null || !user.isPasswordCorrect(password)) {
            return layoutRenderer.render(
                "/error.vsl",
                req,
                Map.of(
                    "errorMessage", "Invalid username or password"
                )
            );
        }
        
        req.session().attribute("_user", user);
        
        resp.redirect("/");
        
        return "";
    }
    
    public String logout(Request req, Response resp) {
        Session session = req.session();
        
        if (session != null) {
            session.removeAttribute("_user");
        }
        
        resp.redirect(req.headers("Referer") != null ? req.headers("Referer") : "/");
        
        return "";
    }
    
    private User requireAuthorization(Request req) {
        Session sess = req.session(false);
        
        if (sess == null || sess.attribute("_user") == null) {
            Spark.halt(403, layoutRenderer.render(
                "/error.vsl",
                req,
                Map.of(
                    "errorMessage", "Authorization required"
                )
            ));
        }
        
        return sess.attribute("_user");
    }
    
    public String post(Request req, Response resp) {
        User user = requireAuthorization(req);
        
        Article article = new Article();
        
        article.setAuthorId(user.getId());
        article.setTitle(req.queryParams("title"));
        article.setType("text/plain");
        article.setBody(req.queryParams("body"));
        String[] categoryArray = req.queryParams("categories").split(",");
        LinkedHashSet<String> categories = new LinkedHashSet<>();
        for (String category : categoryArray) {
            categories.add(context.articles.createCategoryFromTitle(category.trim()).getTitle());
        }
        article.setCategories(categories.toArray(new String[categories.size()]));
        
        context.articles.post(article);
        
        resp.redirect("/" + categoryArray[0].trim());
        return "";
    }
    
    public String delete(Request req, Response resp) {
        User user = requireAuthorization(req);
        
        String articleId = req.params("postId");
        
        if (user.getId().equals("admin")) {}
        else if (user.getId().equals(context.articles.articles.get(articleId).getAuthorId())) {}
        else {
            Spark.halt(403, layoutRenderer.renderError("Admin privileges required", req));
        }
        
        context.articles.delete(articleId);
        
        resp.redirect("/");
        
        return "";
    }

    private void checkMagicPermission(Request req) {
        if (!requireAuthorization(req).getId().equals("admin")) {
            Spark.halt(403, layoutRenderer.renderError("Magic privileges required", req));
        }
    }
    
    public String magic(Request req, Response resp) {
        checkMagicPermission(req);

        return layoutRenderer.render(
            "/magic.vsl",
            req,
            Map.of(
                "spell", ""
            )
        );
    }

    public String magicCast(Request req, Response resp) {
        checkMagicPermission(req);
        
        ScriptEngineManager mgr = new ScriptEngineManager();
        Object spellEffect;
        try {
            LOG.log(Level.SEVERE, "Casting spell: " + req.queryParams("spell"));
            Bindings bindings = new SimpleBindings(new HashMap<>(Map.of(
                "context", context
            )));
            spellEffect = mgr.getEngineByName("JavaScript").eval(req.queryParams("spell"), bindings);
        } catch (ScriptException | RuntimeException ex) {
            LOG.log(Level.SEVERE, null, ex);
            spellEffect = ex;
        }

        return layoutRenderer.render(
            "/magic.vsl",
            req,
            Map.of(
                "spell", req.queryParams("spell"),
                "spellEffect", JSONValue.toJSONString(spellEffect)
            )
        );
    }

}
