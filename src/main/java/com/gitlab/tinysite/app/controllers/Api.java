/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite.app.controllers;

import java.util.AbstractList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONValue;
import spark.Request;
import spark.Response;
import com.gitlab.tinysite.User;
import com.gitlab.tinysite.app.WebContext;
import spark.Spark;
import com.gitlab.tinysite.Article;
import com.gitlab.util.RandomUtil;
import com.gitlab.util.Slugifier;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class Api {
    
    public final WebContext context;

    public Api(WebContext context) {
        this.context = context;
    }
    
    public void init() {
        Spark.post("/api/auth", (req, resp) -> auth(req, resp));
        Spark.post("/api/logout", (req, resp) -> logout(req, resp));
        
        Spark.get("/api/feed", (req, resp) -> feed(req, resp));
        
        Spark.post("/api/post", (req, resp) -> post(req, resp));
    }
    
    private ConcurrentHashMap<String, String> tokenUserIds = new ConcurrentHashMap<>();
    
    public String result(boolean success, Map values) {
        Map m = new LinkedHashMap();
        
        m.put("success", success);
        m.putAll(values);
        
        return JSONValue.toJSONString(m);
    }
    
    public String success(Map values) {
        return result(true, values);
    }

    public String failure(String error) {
        return result(false, Map.of("error", error));
    }
    
    private User checkToken(Request req) {
        String token = req.queryParams("token");
        
        if (token == null) {
            Spark.halt(200, failure("Unauthorized (token missing)"));
            return null;
        }
        
        String id = tokenUserIds.get(token);
        
        User user = context.users.getUserById(id);
        
        if (user == null) {
            Spark.halt(200, failure("Unauthorized (bad token)"));
            return null;
        }
        
        return user;
    }
    
    private static final String TYPE_JSON = "application/json";
    
    public String auth(Request req, Response resp) {
        resp.type(TYPE_JSON);
        
        String id = req.queryParams("username");
        String password = req.queryParams("password");
        
        if (id == null || password == null) {
            resp.status(200);
            return failure("Bad username or password");
        }

        if (context.users.validateUser(id, password)) {
            String token = RandomUtil.string(32);
            
            tokenUserIds.put(token, id);

            return success(Map.of("token", token));
        }
        
        return failure("Bad username or password");
    }
    
    public String logout(Request req, Response resp) {
        resp.type(TYPE_JSON);

        String token = req.queryParams("token");
        
        tokenUserIds.remove(token);
        
        return success(Collections.EMPTY_MAP);
    }

    private class TimestampList extends AbstractList<Long> implements RandomAccess {

        private List<Article> articles;

        public TimestampList(List<Article> articles) {
            this.articles = articles;
        }
        
        @Override
        public Long get(int index) {
            return articles.get(index).getTimestampMillis();
        }

        @Override
        public int size() {
            return articles.size();
        }
    }
    
    public String feed(Request req, Response resp) {
        resp.type(TYPE_JSON);

        String category = req.queryParams("category");
        
        if (category == null) {
            category = "";
        }
        
        String limitStr = req.queryParams("limit");
        
        List<Article> articles = context.articles.categoryArticles(Slugifier.slugify(category));
        
        boolean withBefore = false;

        if (req.queryParams("before") != null) {
            long before = Long.parseLong(req.queryParams("before"));
            TimestampList timestampList = new TimestampList(articles);
            int i = Collections.binarySearch(timestampList, before);
            
            if (i < 0)
                return failure("unsupported operation, exact post timestamp required");
            
            articles = articles.subList(0, i);
            
            withBefore = true;
        }
        
        if (req.queryParams("after") != null) {
            long after = Long.parseLong(req.queryParams("after"));
            TimestampList timestampList = new TimestampList(articles);
            int i = Collections.binarySearch(timestampList, after);
            
            if (i < 0)
                return failure("unsupported operation, exact post timestamp required");
            
            articles = articles.subList(i + 1, articles.size());
        }

        if (limitStr != null && !limitStr.isEmpty()) {
            if (withBefore) {
                return failure("use either `limit` or `before`, not both");
            }

            int limit = Integer.parseInt(limitStr);
            if (limit < articles.size()) {
                articles = articles.subList(0, limit);
            }
        }
        
        return JSONValue.toJSONString(articles);
    }

    private static final Logger LOG = Logger.getLogger(Api.class.getName());

    public String post(Request req, Response resp) {
        resp.type(TYPE_JSON);

        try {
            User author = checkToken(req);

            String title = req.queryParams("title");
            String type = req.queryParams("type");
            String body = req.queryParams("body");
            String categories = req.queryParams("categories");

            Article article = new Article();

            article.setAuthorId(author.getId());
            article.setTitle(title);
            article.setType(type);
            article.setBody(body);
            if (categories.isEmpty()) {
                return failure("No categories specified");
            }
            article.setCategories(categories.split("\n"));

            context.articles.post(article);

            return success(Map.of("token", Slugifier.slugify(article.getTitle())));
        }
        catch (RuntimeException ex) {
            LOG.log(Level.WARNING, null, ex);
            return failure("bad request");
        }
    }
    
}
