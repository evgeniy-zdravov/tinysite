/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite;

import com.twitter.Autolink;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONAware;
import org.json.simple.JSONValue;
import com.gitlab.util.Base58;
import com.gitlab.util.ExternalizeUtil;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class Article implements Externalizable, JSONAware {
    
    private String authorId;
    private String title;
    private String type;
    private String body;
    private String[] categories = EMPTY_STRING_ARRAY;
    private long timestampMillis = 0;
    
    private static String[] EMPTY_STRING_ARRAY = new String[0];

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        LinkedHashMap m = new LinkedHashMap();
        
        m.put("authorId", authorId);
        m.put("title", title);
        m.put("type", type);
        m.put("body", body);
        m.put("categories", Arrays.asList(categories));
        m.put("timestamp", timestampMillis);
        
        ExternalizeUtil.writeAsJSON(out, m);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Map<String, Object> m = ExternalizeUtil.readInJSON(in);
        
        authorId = (String) m.get("authorId");
        title = (String) m.get("title");
        type = (String) m.get("type");
        body = (String) m.get("body");
        categories = ((List<String>) m.get("categories")).toArray(new String[0]);
        timestampMillis = ((Number)m.get("timestamp")).longValue();
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public long getTimestampMillis() {
        return timestampMillis;
    }

    public void setTimestampMillis(long timestamp) {
        this.timestampMillis = timestamp;
    }
    
    public Date getTimestamp() {
        return timestampMillis == 0 ? null : new Date(timestampMillis);
    }
    
    public String calculateHash() {
        MessageDigest md;

        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            throw new Error(ex);
        }
        
        if (title != null) {
            md.update(title.getBytes(StandardCharsets.UTF_8));
        }
        
        return Base58.encode(md.digest(body.getBytes(StandardCharsets.UTF_8)));
    }
    
    public String calculateId() {
        if (timestampMillis == 0) {
            throw new IllegalStateException();
        }

        return calculateHash() + "-" + timestampMillis;
    }
    
    private String cachedHtml = null;
    
    public String getHtml() {
        switch (type) {
            case "text/html":
                return body;
            default:
                if (cachedHtml != null)
                    return cachedHtml;
                
                String s = body;
                
                s = body
                    .replace("&", "&amp;")
                    .replace("<", "&lt;")
                    .replace(">", "&gt;")
                    .replace("\n", "</p><p>")
                    .replace("<p></p>", "");
                
                s = new Autolink().autoLinkURLs(s);
                
                return cachedHtml = "<p>"
                    + s
                    + "</p>";
        }
    }

    @Override
    public String toJSONString() {
        Map m = new LinkedHashMap();
        
        m.put("author", authorId);
        if (title != null && !title.isEmpty()) {
            m.put("title", title);
        }
        m.put("type", type);
        m.put("body", body);
        m.put("date", timestampMillis);
        m.put("categories", Arrays.asList(categories));
        
        return JSONValue.toJSONString(m);
    }

}
