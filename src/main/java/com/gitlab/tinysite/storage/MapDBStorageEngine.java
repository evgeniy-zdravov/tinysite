/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite.storage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import com.gitlab.tinysite.StorageEngine;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class MapDBStorageEngine implements StorageEngine {

    public DB db;
    Thread commiterThread;
    
    public static MapDBStorageEngine file(File f) {
        DB db = DBMaker
                .fileDB(f)
                    .fileMmapEnableIfSupported()
                    .closeOnJvmShutdown()
                    .transactionEnable()
                        .make();
        
        return new MapDBStorageEngine(db);
    }

    public MapDBStorageEngine(DB db) {
        this.db = db;
        
        commiterThread = new Thread(() -> {
            try {
                for(;;) {
                    try {
                        Thread.sleep(30000L);
                        db.commit();
                    }
                    catch (RuntimeException ex) {
                    }
                }
            }
            catch (InterruptedException ex) {
                db.commit();
            }
        }, "MapDB commiter");
        
        commiterThread.start();
    }
    
    @Override
    public <K, V> Map<K, V> map(String id) {
        return (Map<K, V>) db.hashMap(id).createOrOpen();
    }

    @Override
    public <V> Set<V> set(String id) {
        return (Set<V>) db.hashSet(id).createOrOpen();
    }

    @Override
    public <V> List<V> list(String id) {
        return (List<V>) db.indexTreeList(id).createOrOpen();
    }

    @Override
    public void close() throws IOException {
        commiterThread.interrupt();
        db.close();
    }
    
}
