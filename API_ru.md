# Авторизация

POST /api/auth

## Запрос

* username
* password

пример:

    username=vasya&password=123yadebil

## Ответ

    { "success": true, "token": "aiLu9mae7er5eexei4quehaevieYiduquae2fahGh5onaighiep3waiGuNeiThuo" }

    или

    { "success": false, "error": "пнх" }

# Получить список постов

GET /api/feed
или
GET /api/feed?category=новости

* category - категория
* after - мин. дата
* before - макс. дата
* limit - макс. кол-во

пагинация тут наверное будет не к месту

## Ответ

    [
        {
            "author": "vasya",
            "title": "Вася устал",
            "type": "text/plain",
            "body": "Вася устал и пошел спать",
            "date": 1537052572300,
            "categories": ["новости"]
        },
        {
            "author": "vasya",
            "title": "Вася поспал",
            "type": "text/plain",
            "body": "Вася отдавил слепого",
            "date": 1538052572300,
            "categories": ["новости"]
        }
    ]

    type - "text/plain" пока, в перспективе "text/markdown" или "text/html"
    title - опциональное поле (пока не решил, нужно ли оно)
    date - миллисекунды от начала эпохи

# Запостить

POST /api/post

## Поля запроса

* token - токен из авторизации
* title - заголовок
* type - тип контента
* body - контент
* categories - категории, разделённые \n (из js как-то так: ). для каждого поста должна быть указана как минимум 1 категория

## Ответ

    { "success": true, "date": 1537052572300, "slug": "vasya-ustal" }

    или

    { "success": false, "error": "пнх" }
