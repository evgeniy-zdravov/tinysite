/*
 * Copyright 2018 Evgeniy Zdravov <vkbaakre@vfemail.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 */
package com.gitlab.tinysite;

import java.util.Properties;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.junit.Test;
import static org.junit.Assert.*;
import com.gitlab.tinysite.app.WebContext;
import com.gitlab.tinysite.storage.CollectionsStorageEngine;

/**
 *
 * @author Evgeniy Zdravov <vkbaakre@vfemail.net>
 */
public class UserTest {
    
    @org.junit.Test
    public void testReadWriteExternal() throws Exception {
        User u = new User();
        
        u.setId("onetwothree");
        u.setPassword("123123123123");
        
        ByteArrayOutputStream bufOut = new ByteArrayOutputStream();
        
        ObjectOutputStream out = new ObjectOutputStream(bufOut);
        
        out.writeObject(u);
        
        out.close();
        
        ByteArrayInputStream bufIn = new ByteArrayInputStream(bufOut.toByteArray());
        
        ObjectInputStream in = new ObjectInputStream(bufIn);
        
        u = (User) in.readObject();
        
        assertEquals("onetwothree", u.getId());
        assertFalse(u.isPasswordCorrect("12"));
        assertTrue(u.isPasswordCorrect("123123123123"));
    }

    @org.junit.Test
    public void testPassword() {
        User u = new User();
        
        u.setPassword("123123123123");
        
        assertFalse(u.isPasswordCorrect("12"));
        assertTrue(u.isPasswordCorrect("123123123123"));
    }
    
    DbUsers db = new WebContext(new Properties(), new CollectionsStorageEngine()).users;
    
    @Test
    public void testDb() {
        assertFalse(db.userExists("test.one"));

        User u;

        u = new User();

        u.setId("test.one");
        u.setPassword("123123123123");

        db.createUser(u);

        assertTrue(db.userExists("test.one"));

        u.setPassword("123123123123!");

        db.updateUser(u);

        assertTrue(db.userExists("test.one"));
        //assertTrue(db.getUserById("test.one", u).isPasswordCorrect("123123123123!"));
        assertTrue(db.getUserById("test.one").isPasswordCorrect("123123123123!"));
    }
    
}
